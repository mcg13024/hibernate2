package edu.byui.mcgrew.hibernatetest.java;

import java.util.*;


public class Main {

    public static void main(String[] args) {

        TestDAO t = TestDAO.getInstance();

        List<Customer> c = t.getCustomers();
        for (Customer i : c) {
            System.out.println(i);
        }
        System.out.println(t.getCustomer(1));
    }}