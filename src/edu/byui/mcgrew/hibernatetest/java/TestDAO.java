package edu.byui.mcgrew.hibernatetest.java;

import java.util.List;
import java.lang.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class TestDAO {

    SessionFactory factory;
    Session session = null;

    private static TestDAO single_instance = null;

    private TestDAO()

    {
        factory = HibernateUtils.getSessionFactory();
    }

    public static TestDAO getInstance()
    {
        if (single_instance == null){
            single_instance = new TestDAO();
        }
        return single_instance;
    }
    public List<Customer> getCustomers(){
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from edu.byui.mcgrew.hibernatetest.java.Customer";
            List<Customer> cs = (List<Customer>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }}
    public Customer getCustomer(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from .java.Customer where id=" + Integer.toString(id);
            Customer c = (Customer)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        } }}
